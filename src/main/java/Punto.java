public class Punto {

    private double value_a;
    private double value_b;


    public Punto() {
    }

    public Punto(double value_a, double value_b) {
        this.value_a = value_a;
        this.value_b = value_b;
    }

    public double getValue_a() {
        return value_a;
    }

    public void setValue_a(double value_a) {
        this.value_a = value_a;
    }

    public double getValue_b() {
        return value_b;
    }

    public void setValue_b(double value_b) {
        this.value_b = value_b;
    }

}
