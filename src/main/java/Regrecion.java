import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Regrecion {

    private Dataset dataset;
    private List<Double> list_value_a = new ArrayList<Double>();
    private List<Double> list_value_b = new ArrayList<Double>();
    private List<Double> sum_value_b_x_value_a = new ArrayList<>();
    private double n_row;
    private double pendiente;

    public Regrecion() {
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public List<Double> getList_value_a() {
        return list_value_a;
    }

    public void setList_value_a(List<Double> list_value_a) {
        this.list_value_a = list_value_a;
    }

    public List<Double> getList_value_b() {
        return list_value_b;
    }

    public void setList_value_b(List<Double> list_value_b) {
        this.list_value_b = list_value_b;
    }

    public boolean loadValues(Dataset dataset) {

        if(!Optional.ofNullable(dataset).isPresent()){
            throw new NullPointerException("Dataset nulo");
        }else{
            for (Punto p: dataset.punto) {
                this.list_value_a.add(p.getValue_a());
                this.list_value_b.add(p.getValue_b());
                n_row+=1.0;
            }
            if(list_value_a.isEmpty() || list_value_b.isEmpty()){
                return true;
            }else {
                return false;
            }
        }
    }

    private double summatory(List<Double> values){
        double sum = values.stream().mapToDouble(n -> n).sum();
        return sum;
    }
    private double summatoryCross(List<Double> values_a, List<Double> values_b){
        for (int i = 0; i <= n_row-1; i++) {
            sum_value_b_x_value_a.add((values_a.get(i)*values_b.get(i)));
        }
        return summatory(sum_value_b_x_value_a);
    }
    private double summatoryPow2(){
        double tmp = list_value_a.stream().mapToDouble(n -> Math.pow(n,2)).sum();
        return tmp;
    }

    private double pendiente() {
        double a_tmp = ((n_row*summatoryCross(list_value_a,list_value_b))-(summatory(list_value_a)*summatory(list_value_b)));
        double b_tmp = ((n_row*summatoryPow2())-Math.pow(summatory(list_value_a),2));
        pendiente = a_tmp/b_tmp;
        return a_tmp/b_tmp;
    }

    private double prom(List<Double> value_b){
        return summatory(value_b)/n_row;
    }

    public String calculoFormula() {
        pendiente();
        double aditional_value = prom(list_value_b) - (pendiente*prom(list_value_a));
        return ("formula: y="+aditional_value+"+"+pendiente+"X");

    }
}
