import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Dataset {

    private Long Id;
    private String name;
    List<Punto> punto = new ArrayList<>();

    public Dataset() {
    }

    public Dataset(Long id, String name) {
        Id = id;
        this.name = name;
        this.punto = punto;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Punto> getPunto() {
        return punto;
    }

    public void setPunto(List<Punto> punto) {
        this.punto = punto;
    }

    public boolean agregarPuntos(Punto p) {
        if(!Optional.ofNullable(p).isPresent()){
            throw new NullPointerException("Punto nulo, revise los valores de los datos a agregar");
        }else{
            this.punto.add(p);
            return true;
        }

    }
}
