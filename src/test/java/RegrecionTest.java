import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegrecionTest {

    private Dataset dataset;
    private Punto p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14;
    private Regrecion regrecion =new Regrecion();

    @BeforeEach
    void setUp() {
        p1 = new Punto(0.0,0.6928);
        p2 = new Punto(1.0,0.2249);
        p3 = new Punto(2.0,0.5746);
        p4 = new Punto(3.0,0.6240);
        p5 = new Punto(4.0,0.0976);
        p6 = new Punto(5.0,0.0710);
        p7 = new Punto(6.0,0.1333);
        p8 = new Punto(7.0,0.2069);
        p9 = new Punto(8.0,0.9619);
        p10 = new Punto(9.0,0.4381);
        p11 = new Punto(10.0,0.0597);
        p12 = new Punto(11.0,0.9129);
        p13 = new Punto(12.0,0.8944);
        p14 = new Punto(13.0,0.5213);
        dataset = new Dataset((long)1,"dataset 1");
        dataset.agregarPuntos(p1);
        dataset.agregarPuntos(p2);
        dataset.agregarPuntos(p3);
        dataset.agregarPuntos(p4);
        dataset.agregarPuntos(p5);
        dataset.agregarPuntos(p6);
        dataset.agregarPuntos(p7);
        dataset.agregarPuntos(p8);
        dataset.agregarPuntos(p9);
        dataset.agregarPuntos(p10);
        dataset.agregarPuntos(p11);
        dataset.agregarPuntos(p12);
        dataset.agregarPuntos(p13);
        dataset.agregarPuntos(p14);

    }

    @Test
    @DisplayName("Test Carga valores dataset")
    public void cargaTest(){
        assertEquals(false,regrecion.loadValues(dataset));
    }
    @Test
    @DisplayName("Test Carga dataset nulo")
    public void nullRegrecionCargaTest(){
        Throwable nulo = assertThrows(NullPointerException.class,()->{
            regrecion.loadValues(null);
        });
    }

    @Test
    @DisplayName("Test calculo formula")
    public void calculoFormulaTes(){
        regrecion.loadValues(dataset);
        assertEquals("formula: y=0.3341228571428572+0.019073406593406582X",regrecion.calculoFormula());

    }
}