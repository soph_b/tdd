import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName("Prueba clase Dataset")
class DatasetTest {

    private Dataset dataset;
    private Punto p1,p2;

    @BeforeEach
    @DisplayName("Carga de datos iniciales")
    void setUp() {
        p1 = new Punto(0.1,0.2);
        p2 = new Punto(0.3,3.6);
        dataset = new Dataset((long)1,"dataset 1");
    }

    @Test
    @DisplayName("Prueba de agregar puntos al dataset")
    public void testAgregarPuntos(){
        assertEquals(true,dataset.agregarPuntos(p1));
        assertEquals(true,dataset.agregarPuntos(p2));
    }
    @Test
    @DisplayName("Agregar punto null")
    public void testAgregarNulo(){

        Throwable nulo = assertThrows(NullPointerException.class,()->{
            dataset.agregarPuntos(null);
        });
    }
}